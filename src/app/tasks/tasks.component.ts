import { Component, OnInit } from '@angular/core';

import { Task } from './shared/task.model';
import { TaskService } from './shared/task.service';

const TASKS: Array<Task> = [
  { id: 1, title: 'tarefa 1' },
  { id: 2, title: 'tarefa 2' },
  { id: 3, title: 'tarefa 3' },
  { id: 4, title: 'tarefa 4' },
  { id: 5, title: 'tarefa 5' }

];

@Component({
  selector: 'tasks',
  templateUrl: './tasks.component.html',
})

export class TasksComponent implements OnInit {
  public tasks: Array<Task>;
  public selectedTask: Task;

  public constructor(private taskService: TaskService) {
    this.taskService = taskService;
  }

  public ngOnInit() {
    this.taskService.getTasks()
      .subscribe(
        tasks => this.tasks = tasks,
        error => alert('Ocorreu um erro no servidor, tente mais tarde')
      );
  }

  public onSelect(task: Task): void {
    this.selectedTask = task;
  }
}
