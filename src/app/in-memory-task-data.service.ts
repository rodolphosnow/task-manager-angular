import { Injectable } from '@angular/core';

import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable()

export class InMemoryTaskDataService implements InMemoryDbService {
  public createDb() {
    let tasks = [
      {id: 1, title: 'teste'},
      {id: 2, title: 'teste'},
      {id: 3, title: 'teste'}
    ];
    return { tasks };
  }
}

