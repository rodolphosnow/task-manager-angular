import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Task } from './task.model';
import { Observable, of } from 'rxjs';
import { map, filter, scan } from 'rxjs/operators';

const TASKS: Array<Task> = [
  {id: 1, title: 'teste'},
  {id: 2, title: 'teste'},
  {id: 3, title: 'teste'}
];

@Injectable()

export class TaskService {
  public tasksUrl = 'api/tasks';

  public constructor(private http: Http){}

  public getTasks(): Observable<Task[]> {
    return this.http.get(this.tasksUrl)
    .pipe( map((response: Response) => response.json().data as Task[]) );
  }

  // public getTasks(): Promise<Task[]> {
  //   let promise = new Promise(function(resolve, reject) {
  //     if (TASKS.length > 0) {
  //       resolve(TASKS);
  //     } else {
  //       let error_msg = "Not found"
  //       reject(error_msg);
  //     }
  //   })
  //   return promise;
  // }

  public getImporantTasks(): Observable<Task[]> {
    return this.getTasks().pipe( map(tasks => tasks.slice(0, 2)) );
    // return Promise.resolve(TASKS.slice( 0, 2 ));
  }

  public getTask(id: number): Observable<Task> {
    const url = `${this.tasksUrl}/${id}`;
    return this.http.get(url)
      .pipe( map( (response: Response ) => response.json().data as Task) );
  }
}
