import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, of } from 'rxjs';
import { switchMap, map, filter, scan } from 'rxjs/operators';
// import { switchMap, map } from 'rxjs/operators';
// import { Observable, interval, pipe } from 'rxjs';


import { Task } from '../shared/task.model';
import { TaskService } from '../shared/task.service';

@Component({
  selector: 'task-detail',
  templateUrl: './task-detail.component.html'
})

export class TaskDetailComponent implements OnInit {
  public task: Task;

  public constructor(
    private taskService: TaskService,
    private route: ActivatedRoute,
    private location: Location
  ){ }

  public ngOnInit() {
    this.route.params
      .pipe(
      switchMap( (params: Params) => this.taskService.getTask(+params['id']) ) )
        .subscribe(task => this.task = task);
  }

  // public ngOnInit() {
  //   this.route.params
  //     .subscribe((params: Params) => {
  //       this.taskService.getTask(+params['id'])
  //         .then(task => this.task = task);
  //     });
  // }

  public goBack() {
    this.location.back();
  }
}
